from __future__ import division
from ROOT import TFile, TTree, TCanvas, RooFit, TH1F, TH2D, RooRealVar,RooDataHist,RooGaussian, RooVoigtian, RooGenericPdf, RooAddPdf
from array import array

def fit_data( file_path, cuts):
    data_file = TFile(file_path)
    data_tree = data_file.Get('DecayTree')
    data_hist = TH1F('data_hist', 'data_hist', 100, 2850, 3350)
    data_tree.Draw('Jpsi_M>>data_hist', cuts, 'goff')

    Jpsi_M = RooRealVar('Jpsi_M', 'Jpsi_M', 2850, 3350)

    exp1 = RooRealVar('exp1', 'exp1', 0.01, 0 , 1)
    pol0 = RooRealVar('pol0', 'pol0',30, 0, 50)
    pol1 = RooRealVar('pol1', 'pol1', 1,-7 ,5)
    pol2 = RooRealVar('pol2', 'pol2', 1, -3 , 3)
    pol3 = RooRealVar('pol3', 'pol3', 0.5, -1.5 , 1.5)

    mean = RooRealVar('mean', 'Mean of Gauss', 3097,3090, 3105)
    sigma = RooRealVar('sigma', 'width of Gauss', 8, 5, 12)
    mean_voig = RooRealVar('mean_voig', 'mean_voig', 2983 )
    width_voig = RooRealVar('width_voig', 'width_voig', 30)
    nBckgr = RooRealVar('nBckgr', 'num of bckgr', 1e3, 0, 1e6)
    nJpsi = RooRealVar('nJpsi', 'nJpsi', 100, 0, 1e5)
    nEtac = RooRealVar('nEtac', 'nEtac', 10, 0 , 1e5)

    data = RooDataHist('data', 'data set for j/psi', RooArgList(Jpsi_M), data_hist)

    gauss = RooGaussian('gauss','gauss', Jpsi_M, mean, sigma)
    voigtian = RooVoigtian('voig', 'voig', Jpsi_M, mean_voig, width_voig, sigma)
    pdf = RooGenericPdf(' pdf ' , ' pdf ', 'TMath::Exp(-@0*@1)*( @0**2*@4 + @0*@2 + @3)' , RooArgList(Jpsi_M, exp1, pol1, pol0, pol2) ) 
    #( @0**2*@4 + @0*@2 + @3 + @0**3*@5)
    model = RooAddPdf( ' model ' ,' model ', RooArgList( gauss, pdf), RooArgList(nJpsi, nBckgr) )

    model.fitTo(data)

    xframe = Jpsi_M.frame()
    data.plotOn(xframe)
    model.plotOn(xframe)

    #canvas.cd(i+1)
    xframe.Draw()

    sigma_val = int(RooRealVar.getValV(sigma))
    mean_val = int(RooRealVar.getValV(mean))

    min_range = mean_val - 2*sigma_val
    max_range = mean_val + 2*sigma_val

    Jpsi_M.setRange('data_range', min_range, max_range)  
    argSet = RooArgSet(Jpsi_M)  
    value = pdf.createIntegral(argSet, RooFit.NormSet(argSet), RooFit.Range('data_range'))
    print( 'VALUE: %f' %(value.getVal()) )    
    #value = gauss.createIntegral(argSet)
    
    background = float(RooRealVar.getValV(nBckgr))
        
    #value = gauss.createIntegral(argSet)
    background = background*value.getVal() 
    print('Norm INT = %f' %(value.getVal())) 
    print('DATA VALUE BACKGROUND: %f' %(background))
    del data_hist
    del data_file
    del data_tree 
    return background
    
def fit_MC(file_path, cuts):
    mc_file = TFile(file_path)
    mc_tree = mc_file.Get('DecayTree')
    
    mc_hist = TH1F('mc_hist', 'mc_hist', 100, 2850, 3350)
    mc_tree.Draw('Jpsi_M>>mc_hist', cuts, 'goff')

    Jpsi_M_mc = RooRealVar('Jpsi_M_mc', 'Jpsi_M_mc', 2850, 3350)
    mean_mc = RooRealVar('mean_mc', 'Mean of Gauss', 3097)
    sigma_mc = RooRealVar('sigma_mc', 'width of Gauss', 10, 5 , 15)
    nJpsi_mc = RooRealVar('nJpsi_mc', 'nJpsi', 100, 0, 1e5)
    
    gauss_mc = RooGaussian('gauss_mc','gauss', Jpsi_M_mc, mean_mc, sigma_mc)
    data_mc = RooDataHist('data', 'MC for j/psi', RooArgList(Jpsi_M_mc), mc_hist)
    model_mc = RooAddPdf('model_mc', 'model_mc', RooArgList(gauss_mc), RooArgList(nJpsi_mc) )
 
    model_mc.fitTo(data_mc)
     
    signal = float(RooRealVar.getValV(nJpsi_mc))
    signal = signal*0.21
    print("MONTE CARLO %f" %signal)
    del mc_file
    del mc_tree
    del mc_hist
    return signal
    
def draw_hist(hist_name, signal, n_range):
    canvas = TCanvas('canvas','', 800, 400)
    hist = TH1F('%s'%hist_name,'%s'%hist_name, len(n_range), n_range[0],n_range[len(n_range-1)])
    for i in range(len(n_range)):
        hist.SetBinContent(i,signal[i])
    hist.SetMarketStyle(20)
    hist.Draw('LP')
    canvas.SaveAs('/afs/cern.ch/work/t/tfedorch/private/analysis/%s.pdf'%hist_name)
    del canvas
    del hist

cuts_data = ' Jpsi_FDCHI2_OWNPV>81 && ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && PiP_PT > 500 && PiM_PT > 500 &&  PiP_IPCHI2_OWNPV > 9 && PiM_IPCHI2_OWNPV > 9 && PiP_ProbNNpi > 0.2 && PiM_ProbNNpi > 0.2 && (Psi_M - Jpsi_M - f0_M) < 300 && Jpsi_Hlt2Topo2BodyDecision_TOS && Jpsi_Hlt1TrackMVADecision_TOS && Jpsi_L0HadronDecision_TOS'
#&& (Psi_M - Jpsi_M - f0_M) < 300
cuts_mc = ' Jpsi_FDCHI2_OWNPV>81 && ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && Jpsi_Hlt2Topo2BodyDecision_TOS && Jpsi_Hlt1TrackMVADecision_TOS && Jpsi_L0HadronDecision_TOS'
#&& Jpsi_Hlt2Topo2BodyDecision_TOS && Jpsi_Hlt1TrackMVADecision_TOS && Jpsi_L0HadronDecision_TOS
#Jpsi_FDCHI2_OWNPV>81 
#n = 10
#n = 15
n = 6
m = 6
s = array('d', [0]*n)
b = array('d', [0]*n)
#par_file = open('output', 'w')

canvas = TCanvas('canvas','', 800, 400)
h2d = TH2D('h2d', 'Significance', 6, 1000, 7000, 6 ,1000, 4000)
#h2d.SetStats(False)
for i in range(n):
    for j in range(m):
        cuts_data = cuts_data + ' && Jpsi_PT>' + str(1000 + 1000*i) + ' && ProtonP_PT >{0} && ProtonM_PT >{0}'.format(1000 + j*500)
        cuts_mc = cuts_mc + ' && Jpsi_PT>' + str(1000 + 1000*i) + ' && ProtonP_PT >{0} && ProtonM_PT >{0}'.format(1000 + j*500)
    #' && Jpsi_PT>' + str(500*(i+3))
    #' && Jpsi_FDCHI2_OWNPV>' + str(9*(i+4))
    #' && Jpsi_ENDVERTEX_CHI< ' + str(i + 2)
    # str(500*(i+3))

        cut1 = TCut(cuts_data)
        cut2 = TCut(cuts_mc)

        background = fit_data('/afs/cern.ch/work/t/tfedorch/private/analysis/merged_tuples.root', cuts_data)
        signal = fit_MC('/afs/cern.ch/work/t/tfedorch/private/analysis/Jpsi_Sec_Detached_allSelected_AddBr.root', cuts_mc)
        sign = signal/( TMath.Sqrt(signal + background) )
        bin = h2d.GetBin(i+1, j+1)
        h2d.SetBinContent(bin, sign)


h2d.Draw('COLZ')
canvas.SaveAs('/afs/cern.ch/work/t/tfedorch/private/analysis/hist_2D_PT.pdf')

