def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")	 
    tuple.TupleToolRecoStats.Verbose=True


    # TISTOS for Psi
    from Configurables import TupleToolTISTOS, TupleToolDecay
    from Configurables import LoKi__Hybrid__TupleTool
    tuple.addTool(TupleToolDecay, name = 'Psi')
    tuple.Psi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPsi" ]
    tuple.Psi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPsi" )
    tuple.Psi.TupleToolTISTOSForPsi.Verbose=True
    tuple.Psi.TupleToolTISTOSForPsi.TriggerList = myTriggerList
    LoKi_Psi=LoKi__Hybrid__TupleTool("LoKi_Psi")
    LoKi_Psi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Psi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Psi"]
    tuple.Psi.addTool(LoKi_Psi)

	
	
    tuple.addTool(TupleToolDecay, name = 'Etac')
    tuple.Etac.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForEtac" ]
    tuple.Etac.addTool(TupleToolTISTOS, name="TupleToolTISTOSForEtac" )
    tuple.Etac.TupleToolTISTOSForEtac.Verbose=True
    tuple.Etac.TupleToolTISTOSForEtac.TriggerList = myTriggerList
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Etac=LoKi__Hybrid__TupleTool("LoKi_Etac")
    LoKi_Etac.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }    
    tuple.Etac.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Etac"]
    tuple.Etac.addTool(LoKi_Etac)


    tuple.addTool(TupleToolDecay, name = 'f0')
    tuple.f0.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForf0" ]
    tuple.f0.addTool(TupleToolTISTOS, name="TupleToolTISTOSForf0" )
    tuple.f0.TupleToolTISTOSForf0.Verbose=True
    tuple.f0.TupleToolTISTOSForf0.TriggerList = myTriggerList
    LoKi_f0=LoKi__Hybrid__TupleTool("LoKi_f0")
    LoKi_f0.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.f0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_f0"]
    tuple.f0.addTool(LoKi_f0)


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        #,"m_scaled" : "DTF_FUN ( M , False )"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    
    from Configurables import TupleToolSubMass
    tuple.addTool(TupleToolSubMass,name="TupleToolSubMass")
    #tuple.TupleToolSubMass.SetMax = 3
    tuple.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.TupleToolSubMass.Substitution += ["pi+ => K+"]
    tuple.TupleToolSubMass.Substitution += ["pi+ => p+"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/pi+"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/pi+"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => K-/K+"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p~-/p+ => pi-/K+"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => pi+/K-"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => pi+/p~-"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => K+/K-"]
    tuple.TupleToolSubMass.DoubleSubstitution += ["p+/pi- => K+/p~-"]
	# tuple.TupleToolSubMass.OutputLevel = DEBUG
    tuple.ToolList += ["TupleToolSubMass"]
	
# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 "Hlt1AllL0Decision",
                 
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
    ]

year = "2017"
Psi2PhiPhiPiPiLocation = "Phys/Ccbar2PPPiPiLine/Particles"

from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Psi2PhiPhiPiPiLocation) 
inputData = MomentumScaling(inputData, Turbo = False, Year = year)



from PhysSelPython.Wrappers import (
                                    Selection,
                                    SelectionSequence,
                                    DataOnDemand,
                                    AutomaticData,
                                    SimpleSelection
                                    )
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from Configurables import CombineParticles, DaVinci, FilterInTrees, PrintDecayTree

#KaonsFromStr = FilterInTrees("KaonsFromStr",Code="('K+' == ABSID)")
#Kaons = Selection("Kaons",Algorithm=KaonsFromStr,
#                  RequiredSelections=[inputData])

pAlg = FilterInTrees("pAlg",Code="('p+' == ABSID)")
p_sel = Selection("Sel_phi",Algorithm=pAlg,
                  RequiredSelections=[inputData])

PionsFromStr = FilterInTrees("PionsFromStr",Code="('pi+' == ABSID)")
Pions = Selection("Pions",Algorithm=PionsFromStr,
                  RequiredSelections=[inputData])


f0 = CombineParticles('Combine_f0',
                      DecayDescriptor='f_0(980) -> pi+ pi-',
                      DaughtersCuts={"pi+": "(PT > 150) & (TRGHOSTPROB < 0.4) & (MIPCHI2DV(PRIMARY)>4.) ",
				                     "pi-": "(PT > 150) & (TRGHOSTPROB < 0.4) & (MIPCHI2DV(PRIMARY)>4.) " },
                      CombinationCut="AALL",
                      MotherCut="ALL"
                      )
f0_sel = Selection('Sel_f0',
                   Algorithm=f0,
                   RequiredSelections=[Pions])


etac_sel = SimpleSelection('Sel_etac',
                           ConfigurableGenerators.CombineParticles,
                           [p_sel],
                           DecayDescriptor='eta_c(1S) -> p+ p~-',
                           DaughtersCuts={"p+": "(PT > 400) & (TRGHOSTPROB < 0.4) & (MIPCHI2DV(PRIMARY)>4.) ",
				                     "p~-": "(PT > 400) & (TRGHOSTPROB < 0.4) & (MIPCHI2DV(PRIMARY)>4.) "},
                           CombinationCut="(AM > 2.7*GeV) & (AM<6.3*GeV)",
                           MotherCut="ALL"
                           )
etac_seq = SelectionSequence('etac_Seq', TopSelection=etac_sel)


psi_sel = SimpleSelection('Sel_psi',
                           ConfigurableGenerators.CombineParticles,
                           [etac_sel, f0_sel],
                           DecayDescriptor='psi(2S) -> eta_c(1S) f_0(980)',
                           DaughtersCuts={},
                           CombinationCut="(AM > 2.7*GeV) & (AM<100.*GeV)",
                           MotherCut="(BPVDLS > 5) & (VFASPF(VCHI2/VDOF) < 9)"
                           )
psi_seq = SelectionSequence('psi_Seq', TopSelection=psi_sel)



Psi2PpbarPiPiTuple = DecayTreeTuple("Psi2PpbarPiPiTuple")
Psi2PpbarPiPiTuple.Decay = "psi(2S) -> ^(eta_c(1S) -> ^p+ ^p~-) ^(f_0(980) ->^pi+ ^pi-)"
Psi2PpbarPiPiTuple.Inputs = [psi_seq.outputLocation()]





Psi2PpbarPiPiBranches = {
     "ProtonP" :  "psi(2S) -> (eta_c(1S)-> ^p+ p~-) (f_0(980)->pi+ pi-)"
    ,"ProtonM" :  "psi(2S) -> (eta_c(1S)->  p+^p~-) (f_0(980)->pi+ pi-)"
    ,"Etac"    :  "psi(2S) ->^(eta_c(1S)-> p+ p~-) (f_0(980)->pi+ pi-)"
    ,"PiP"     :  "psi(2S) -> (eta_c(1S)-> p+ p~-) (f_0(980)->^pi+ pi-)"
    ,"PiM"     :  "psi(2S) -> (eta_c(1S)-> p+ p~-) (f_0(980)->pi+ ^pi-)"
    ,"f0"      :  "psi(2S) -> (eta_c(1S)-> p+ p~-) ^(f_0(980)->pi+ pi-)"
    ,"Psi"    :  "(psi(2S) -> (eta_c(1S)-> p+ p~-) (f_0(980)->pi+ pi-))"
    }
fillTuple( Psi2PpbarPiPiTuple, Psi2PpbarPiPiBranches, myTriggerList )


# Psi2PhiPhiPiPiTuple = TupleSelection("Psi2PhiPhiPiPiTuple", Psi_seq.outputLocation(), Decay = "psi(2S) -> ^(phi(1020) -> ^K+ ^K-) ^(phi(1020) -> ^K+ ^K-) ^pi+ ^pi-", Branches = Psi2PhiPhiPiPiBranches)
# fillTuple( Psi2PhiPhiPiPiTuple, myTriggerList )





# Psi2PhiPhiPiPiTuple.Inputs = [ inputData ]
# fillTuple( Psi2PhiPhiPiPiTuple, Psi2PhiPhiPiPiBranches, myTriggerList )



#from PhysConf.MicroDST import uDstConf
#uDstConf ("/Event/Charm")


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2PpbarPiPiFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2PPPiPiLineDecision')"""
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Ccbar2PpbarPiPiFilters.filters('Ccbar2PpbarPiPiFilters')
DaVinci().EvtMax = 10000                      # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            psi_seq.sequence(),
                            Psi2PpbarPiPiTuple ]        # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Bhadron"

# Get Luminosity
DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20170721-3"
# DaVinci().CondDBtag = "cond-20161004"


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

from GaudiConf import IOHelper
# Use the local input data
IOHelper().inputFiles([
                       '00071499_00000029_1.bhadron.mdst'
                       ], clear=True)

